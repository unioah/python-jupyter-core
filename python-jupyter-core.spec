%global _empty_manifest_terminate_build 0
Name:		python-jupyter-core
Version:	4.9.1
Release:	1
Summary:	Jupyter core package. A base package on which Jupyter projects rely.
License:	BSD-3-Clause
URL:		https://jupyter.org
Source0:	jupyter_core-4.9.1.tar.gz
BuildArch:	noarch

Requires:	python3-traitlets

%description
Jupyter core package. A base package on which Jupyter projects rely.



%package -n python3-jupyter-core
Summary:	Jupyter core package. A base package on which Jupyter projects rely.
Provides:	python3-jupyter-core
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-jupyter-core
Jupyter core package. A base package on which Jupyter projects rely.



%package help
Summary:	Development documents and examples for jupyter-core
Provides:	python3-jupyter-core-doc
%description help
Jupyter core package. A base package on which Jupyter projects rely.



%prep
%autosetup -n jupyter_core-4.9.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-jupyter-core -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jan 10 2022 Python_Bot <Python_Bot@openeuler.org> - 4.9.1-1
- Package Spec generated
